package com.example.fbloginmvvm.view.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.fbloginmvvm.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
