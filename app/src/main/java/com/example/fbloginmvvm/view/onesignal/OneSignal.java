package com.example.fbloginmvvm.view.onesignal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.fbloginmvvm.R;
import com.example.fbloginmvvm.helper.OneSignalHelper;

public class OneSignal extends AppCompatActivity {

    private String[] playerIds={"90644122-68c6-439d-8f8a-ca01a0f4f8b7"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_signal);


    }

    public void mClick(View view) {
        OneSignalHelper.pushNotificationByPlayerID(playerIds);
    }
}
